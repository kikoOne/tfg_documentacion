(function(window, undefined) {

  var jimLinks = {
    "2c592e6f-154b-4454-a810-e70e6bb7ec04" : {
      "Panel_23" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Label_15" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Label_16" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Panel_25" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Label_20" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Label_21" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "86420ff9-bc12-441d-9b22-3e5d4d10b6d9" : {
      "Rectangle_15" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_3" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Rectangle_16" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_4" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_16" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_24" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_25" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_26" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_27" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_32" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "3d02c1f3-efe2-47e0-af6f-e0720fb665e5" : {
      "Image_1" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ],
      "Button_20" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_17" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Flat-button-white" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Flat-button-white_1" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ]
    },
    "9ea47095-7404-4b59-b320-22b9e13c3cef" : {
      "Panel_6" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_12" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_12" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Label_9" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Label_13" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Label_10" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ]
    },
    "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8" : {
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_20" : [
        "08a36c6e-1bf4-496a-9811-990cef3d6f77"
      ],
      "Text_33" : [
        "3d02c1f3-efe2-47e0-af6f-e0720fb665e5"
      ],
      "Text_34" : [
        "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2"
      ],
      "Panel_12" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_13" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_14" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_22" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_4" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_5" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_7" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Panel_15" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_20" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ]
    },
    "57f728eb-7d77-4798-b0f9-b8cb5c3603a3" : {
      "Image_1" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ],
      "Label_7" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Text_14" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_15" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_36" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_37" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_38" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_40" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2" : {
      "Image_1" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ],
      "Input_2" : [
        "e25ef06f-c8f3-44a3-a41a-828e114a86b6"
      ],
      "Label_13" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_14" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_15" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_16" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_26" : [
        "08a36c6e-1bf4-496a-9811-990cef3d6f77"
      ],
      "Text_33" : [
        "3d02c1f3-efe2-47e0-af6f-e0720fb665e5"
      ],
      "Text_34" : [
        "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2"
      ],
      "Flat-button-white" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ]
    },
    "eb592fcf-1413-4e3f-a779-ce06729a04ac" : {
      "Button_20" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_17" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Flat-button-white" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Flat-button-white_1" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ]
    },
    "eb0af873-c527-4dcc-82e8-5bc7f0961900" : {
      "Button_18" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ]
    },
    "08a36c6e-1bf4-496a-9811-990cef3d6f77" : {
      "Text_16" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_24" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_25" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_26" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_27" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_32" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "0017267f-5805-42e9-87b2-a3bbd04481e9" : {
      "Button_18" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Button_20" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_9" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_10" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_17" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Flat-button-white" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Flat-button-white_1" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ]
    },
    "47a76658-92fb-44ab-b4ce-eedcf5b534c3" : {
      "Label_31" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Label_26" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Label_33" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Label_35" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "65caabef-175e-4c06-b4ce-477ba83a8a43" : {
      "Image_1" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ],
      "Input_2" : [
        "e25ef06f-c8f3-44a3-a41a-828e114a86b6"
      ],
      "Rectangle_6" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Label_13" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_14" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_15" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Label_16" : [
        "86420ff9-bc12-441d-9b22-3e5d4d10b6d9"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_30" : [
        "08a36c6e-1bf4-496a-9811-990cef3d6f77"
      ],
      "Text_33" : [
        "3d02c1f3-efe2-47e0-af6f-e0720fb665e5"
      ],
      "Text_34" : [
        "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2"
      ],
      "Flat-button-white" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ]
    },
    "e25ef06f-c8f3-44a3-a41a-828e114a86b6" : {
      "Panel_6" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_22" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_3" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_10" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Panel_23" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Panel_24" : [
        "31a4073e-24ef-4a90-956b-39ef895d8f72"
      ],
      "Label_14" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Label_16" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Text_7" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_8" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_11" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_12" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_13" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_15" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ]
    },
    "31a4073e-24ef-4a90-956b-39ef895d8f72" : {
      "Image_1" : [
        "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8"
      ],
      "Panel_8" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ],
      "Text_46" : [
        "eb592fcf-1413-4e3f-a779-ce06729a04ac"
      ],
      "Text_47" : [
        "0017267f-5805-42e9-87b2-a3bbd04481e9"
      ],
      "Text_48" : [
        "2c592e6f-154b-4454-a810-e70e6bb7ec04"
      ],
      "Text_49" : [
        "9ea47095-7404-4b59-b320-22b9e13c3cef"
      ],
      "Text_50" : [
        "47a76658-92fb-44ab-b4ce-eedcf5b534c3"
      ],
      "Text_51" : [
        "65caabef-175e-4c06-b4ce-477ba83a8a43"
      ],
      "Text_52" : [
        "08a36c6e-1bf4-496a-9811-990cef3d6f77"
      ],
      "Text_53" : [
        "3d02c1f3-efe2-47e0-af6f-e0720fb665e5"
      ],
      "Text_54" : [
        "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2"
      ],
      "Flat-button-white" : [
        "57f728eb-7d77-4798-b0f9-b8cb5c3603a3"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);