(function(window, undefined) {
  var dictionary = {
    "2c592e6f-154b-4454-a810-e70e6bb7ec04": "Mis categorías",
    "86420ff9-bc12-441d-9b22-3e5d4d10b6d9": "Historial",
    "3d02c1f3-efe2-47e0-af6f-e0720fb665e5": "Añadir ámbito",
    "9ea47095-7404-4b59-b320-22b9e13c3cef": "Mis términos",
    "53984e9e-1aed-4c5c-b4ef-e28b1411a0e8": "Prinicipal",
    "57f728eb-7d77-4798-b0f9-b8cb5c3603a3": "Visualización de Termino",
    "8335f6c4-dcf8-491f-bfb2-edaed9b55ef2": "Índice de ámbitos",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Test",
    "eb592fcf-1413-4e3f-a779-ce06729a04ac": "Añadir categoría",
    "eb0af873-c527-4dcc-82e8-5bc7f0961900": "Login",
    "08a36c6e-1bf4-496a-9811-990cef3d6f77": "Vista de usuarios",
    "0017267f-5805-42e9-87b2-a3bbd04481e9": "Añadir término",
    "47a76658-92fb-44ab-b4ce-eedcf5b534c3": "Mis borradores",
    "65caabef-175e-4c06-b4ce-477ba83a8a43": "Índice de categorías",
    "e25ef06f-c8f3-44a3-a41a-828e114a86b6": "Búsqueda",
    "31a4073e-24ef-4a90-956b-39ef895d8f72": "Visualización de Categoría",
    "87db3cf7-6bd4-40c3-b29c-45680fb11462": "960 grid - 16 columns",
    "e5f958a4-53ae-426e-8c05-2f7d8e00b762": "960 grid - 12 columns",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);